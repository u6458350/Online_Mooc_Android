package com.example.chao.myapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.chao.myapplication.R;
import com.example.chao.myapplication.model.Resource;

import java.util.List;

/**
 * Created by huidh123 on 2017/6/7.
 */

public class ResourceItemAdapter extends BaseAdapter {
    private List<Resource> mData;
    private LayoutInflater mInflater;

    public ResourceItemAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void updateData(List<Resource> data){
        this.mData = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rootView = mInflater.inflate(R.layout.item_resource_layout, null);
        TextView title = (TextView) rootView.findViewById(R.id.title);
        TextView details = (TextView) rootView.findViewById(R.id.details);
        title.setText(mData.get(i).getTitle());
        details.setText(mData.get(i).getDetails());
        return rootView;
    }
}
