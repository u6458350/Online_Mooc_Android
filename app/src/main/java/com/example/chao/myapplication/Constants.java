package com.example.chao.myapplication;

import com.http.User;

/**
 * Created by chao on 2017/5/5.
 */

public class Constants {
    //public static final String IP_ADDRESS = "http://192.168.191.1:8000";//530
    //public static final String IP_ADDRESS = "http://192.168.7.105:8000";/home
    public static final String IP_ADDRESS = "http://10.236.148.236:8000";//517
    private static final String SERVER_URL = IP_ADDRESS + "/app";

    public static final String REGISTER = SERVER_URL + "/register";
    public static final String LOGIN = SERVER_URL + "/login";
    public static final String QUERY_RES_LIST = SERVER_URL + "/getResourcesList";
    public static final String SEARCH = SERVER_URL + "/searchResource";
    public static final String UPDATE_USER_INFO = SERVER_URL + "/updateUserInfo";
    public static final String QUERY_RES_DETAIL = SERVER_URL + "/queryResourceDetail";
    public static final String UPLOAD_RESOURCE = SERVER_URL + "/uploadResource";
    public static final String UPLOAD_URL = SERVER_URL + "/uploadFile";
    public static final String ADD_COMMENT = SERVER_URL + "/commentResource";
    public static final String QUERY_COMMENT = SERVER_URL + "/queryComment";
    public static final String CREATE_TEST = SERVER_URL + "/createTest";
    public static final String QUERY_QUESTION = SERVER_URL + "/queryQuestion";

    public static final String IMAGE_URL = IP_ADDRESS + "/static/uploadFiles";

    //    signed user info
    public static User mSignInUser;
}
