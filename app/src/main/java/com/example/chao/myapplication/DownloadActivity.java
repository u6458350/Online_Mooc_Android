package com.example.chao.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DownloadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }
}
