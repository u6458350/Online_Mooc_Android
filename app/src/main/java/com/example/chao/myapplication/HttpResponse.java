package com.example.chao.myapplication;

import com.google.gson.JsonElement;

import org.json.JSONObject;

/**
 * Created by chao on 2017/5/5.
 */

public class HttpResponse {
    private int code;
    private String msg;
    private JsonElement content;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JsonElement getContent() {
        return content;
    }

    public void setContent(JsonElement content) {
        this.content = content;
    }
}
