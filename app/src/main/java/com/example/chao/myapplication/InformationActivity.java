package com.example.chao.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.http.multipart.FilePart;
import com.android.internal.http.multipart.Part;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.chao.myapplication.Utils.FileUtils;
import com.google.gson.Gson;
import com.http.HttpGetRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InformationActivity extends AppCompatActivity {
    protected static final int CHOOSE_PICTURE = 0;
    protected static final int TAKE_PICTURE = 1;
    private static final int CROP_SMALL_PICTURE = 2;
    protected Uri tempUri;
    RequestQueue queue;
    private RoundImageView headIcon_info;
    private EditText password;
    private EditText gender;
    private EditText email;
    private TextView usernameTextview;
    private Button confirm;
    private RequestQueue mReqQueue;
    private String headiconUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        //获取队列
        queue = Volley.newRequestQueue(InformationActivity.this);

        email = (EditText) findViewById(R.id.et_email);
        gender = (EditText) findViewById(R.id.et_gender);
        password = (EditText) findViewById(R.id.et_password);
        usernameTextview = (TextView) findViewById(R.id.usernameTextview);
        confirm = (Button) findViewById(R.id.confirm);
        mReqQueue = Volley.newRequestQueue(this);
        usernameTextview.setText(this.getString(R.string.my_username, Constants.mSignInUser.getUsername()));
        final String emailStr = Constants.mSignInUser.getEmail();
        this.email.setText(emailStr == null ? "未设定" : emailStr);
        String genderStr = Constants.mSignInUser.getGender();
        this.gender.setText(genderStr == null ? "未设定" : genderStr);
        password.setText(Constants.mSignInUser.getPassword());
        headIcon_info = (RoundImageView) findViewById(R.id.headIcon_info);
        Glide.with(InformationActivity.this).load(Constants.IP_ADDRESS + Constants.mSignInUser.getHeadicon()).into(headIcon_info);
        headIcon_info.setOnClickListener(new ChangeHeadClickListener());

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String updatedEmail = email.getText().toString();
                final String updatedGender = gender.getText().toString();
                final String updatedPassword = password.getText().toString();
                String updatedHeadicon = null;
                if (headiconUrl == null || "null".equals(headiconUrl)) {
                    updatedHeadicon = null;
                } else {
                    updatedHeadicon = headiconUrl;
                }

                if (TextUtils.isEmpty(updatedEmail) || TextUtils.isEmpty(updatedGender) || TextUtils.isEmpty(updatedPassword)) {
                    Toast.makeText(InformationActivity.this, "请填写完整", Toast.LENGTH_LONG).show();
                    return;
                }

                HashMap postParams = new HashMap();
                postParams.put("password", updatedPassword);
                postParams.put("gender", updatedGender);
                postParams.put("email", updatedEmail);
                postParams.put("id", Constants.mSignInUser.getId());
                postParams.put("headicon", updatedHeadicon);

                final String finalUpdatedHeadicon = updatedHeadicon;
                mReqQueue.add(HttpGetRequest.create(Constants.UPDATE_USER_INFO, postParams, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String o) {
                        Gson gson = new Gson();
                        HttpResponse response = gson.fromJson(o, HttpResponse.class);
                        if (response.getCode() == 0) {
                            Toast.makeText(InformationActivity.this, response.getMsg(), Toast.LENGTH_LONG).show();
                            Constants.mSignInUser.setEmail(updatedEmail);
                            Constants.mSignInUser.setGender(updatedGender);
                            Constants.mSignInUser.setPassword(updatedPassword);
                            Constants.mSignInUser.setHeadicon(finalUpdatedHeadicon);
                            InformationActivity.this.finish();
                        } else {
                            Toast.makeText(InformationActivity.this, response.getMsg(), Toast.LENGTH_LONG).show();
                        }
                    }
                }));
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == MainActivity.RESULT_OK) {
            switch (requestCode) {
                case TAKE_PICTURE:
                    cutImage(tempUri); // 对图片进行裁剪处理
                    break;
                case CHOOSE_PICTURE:
                    cutImage(data.getData()); // 对图片进行裁剪处理
                    break;
                case CROP_SMALL_PICTURE:
                    if (data != null) {
                        setImageToView(data); // 让刚才选择裁剪得到的图片显示在界面上
                    }
                    break;
            }
        }
    }

    /**
     * 裁剪图片方法实现
     */
    protected void cutImage(Uri uri) {
        if (uri == null) {
            Log.i("alanjet", "The uri is not exist.");
        }
        tempUri = uri;
        Intent intent = new Intent("com.android.camera.action.CROP");
        //com.android.camera.action.CROP这个action是用来裁剪图片用的
        intent.setDataAndType(uri, "image/*");
        // 设置裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, CROP_SMALL_PICTURE);
    }

    /**
     * 保存裁剪之后的图片数据
     */
    protected void setImageToView(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            // headIcon_info = (RoundImageView) findViewById(R.id.headIcon_info);
            Bitmap mBitmap = extras.getParcelable("data");
            headIcon_info.setBitmap(mBitmap);

            String bmpPath = FileUtils.saveBitmap(this , mBitmap , "temp");
            //构造参数列表
            List<Part> partList = new ArrayList<Part>();
            try {
                partList.add(new FilePart("files", new File(bmpPath)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //生成请求
            queue.add(new MultipartRequest(Constants.UPLOAD_URL, partList.toArray(new Part[partList.size()]), new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    //处理成功返回信息
                    Gson gson = new Gson();
                    HttpResponse response = gson.fromJson(s, HttpResponse.class);
                    if (response.getCode() == 0) {
                        headiconUrl = gson.fromJson(response.getContent(), String.class);
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //处理失败错误信息
                    Log.e("MultipartRequest", error.getMessage(), error);
                    Toast.makeText(getApplication(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }));
        }
    }

    private class ChangeHeadClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(InformationActivity.this);
            builder.setTitle("更换头像");
            String[] items = {"选择本地照片", "拍照"};
            builder.setNegativeButton("取消", null);
            builder.setItems(items, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case CHOOSE_PICTURE: // 选择本地照片
                            Intent openAlbumIntent = new Intent(
                                    Intent.ACTION_GET_CONTENT);
                            openAlbumIntent.setType("image/*");
                            //用startActivityForResult方法，待会儿重写onActivityResult()方法，拿到图片做裁剪操作
                            startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
                            break;
                        case TAKE_PICTURE: // 拍照
                            Intent openCameraIntent = new Intent(
                                    MediaStore.ACTION_IMAGE_CAPTURE);
                            File pic = new File(Environment
                                    .getExternalStorageDirectory(), "temp_image.jpg");
                            tempUri = Uri.fromFile(pic);
                            // 将拍照所得的相片保存到SD卡根目录
                            openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
                            startActivityForResult(openCameraIntent, TAKE_PICTURE);
                            break;
                    }
                }
            });
            builder.show();
            builder.show().dismiss();
        }
    }

}
