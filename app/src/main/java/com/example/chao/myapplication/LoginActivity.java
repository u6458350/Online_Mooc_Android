package com.example.chao.myapplication;
import android.app.Activity;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.http.HttpGetRequest;
import com.http.User;

import java.util.HashMap;


public class LoginActivity extends AppCompatActivity {
    private  Button login,reg;
    private  EditText   username,password;
    /*private SharedPreferences share;//声明SharedPreferences*/
    private RequestQueue mReqQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        intiview();
        mReqQueue = Volley.newRequestQueue(this);

        reg.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                 startActivity(intent);
             }
        });

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String name=username.getText().toString();
                String pass=password.getText().toString();
                if(name.trim().equals("") || pass.trim().equals("")) {
                    Toast.makeText(LoginActivity.this, "用户名和密码不能为空", Toast.LENGTH_LONG).show();
                }

                HashMap postList = new HashMap();
                postList.put("username" , name);
                postList.put("password" , pass);
                mReqQueue.add(HttpGetRequest.create(Constants.LOGIN, postList, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Gson gson = new Gson();
                        HttpResponse response = gson.fromJson(s , HttpResponse.class);

                        if(response.getCode() == 0){
                            Toast.makeText(LoginActivity.this , response.getMsg() , Toast.LENGTH_LONG).show();
                            Constants.mSignInUser = gson.fromJson(response.getContent() , User.class);
                            startActivity(new Intent(LoginActivity.this,ResourceActivity.class));
                        }else{
                            Toast.makeText(LoginActivity.this , response.getMsg() , Toast.LENGTH_LONG).show();
                        }
                    }
                }));
            }
        });
    }


    private void intiview() {
        // TODO Auto-generated method stub
        login=(Button)findViewById(R.id.loginBtn_login);
        reg=(Button)findViewById(R.id.registerBtn_login);
        username=(EditText)findViewById(R.id.editUsername_login);
        password=(EditText)findViewById(R.id.editPassword_login);
    }
}

