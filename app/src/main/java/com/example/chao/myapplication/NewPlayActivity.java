package com.example.chao.myapplication;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

public class NewPlayActivity extends AppCompatActivity {


    RelativeLayout mLayoutGroup = null;
    private PopupWindow mPop;
    private ImageView sense_icon,trans_icon;
    private RelativeLayout main_ll;
    private RelativeLayout sense_layout,trans_layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_play);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        mLayoutGroup = new RelativeLayout(this);
        mLayoutGroup.setBackgroundColor(ContextCompat.getColor(NewPlayActivity.this,R.color.item_imgTag_bg));
        main_ll = (RelativeLayout) findViewById(R.id.view2);
        sense_layout = (RelativeLayout) findViewById(R.id.main_root);
        trans_layout = (RelativeLayout) findViewById(R.id.trans_layout);
        sense_icon = (ImageView) sense_layout.findViewById(R.id.imageView_test);
        trans_icon = (ImageView) trans_layout.findViewById(R.id.transImg);
        sense_icon.setOnLongClickListener(new OnLongClickListener() {

            public boolean onLongClick(View v) {
                initPopWindow();
                mLayoutGroup.removeAllViews();
                PinchableImageView imgBtn = new PinchableImageView(NewPlayActivity.this);
                imgBtn.setImageDrawable(((ImageView) v).getDrawable());
                imgBtn.setX(v.getX());
                imgBtn.setY(v.getY());
                imgBtn.setClickable(true);
                imgBtn.setOnTouchListener(touchListener);
                mLayoutGroup.addView(imgBtn);
                mLayoutGroup.setFocusableInTouchMode(true);
                imgBtn.requestFocus();
                mPop.showAtLocation(
                        NewPlayActivity.this.sense_layout,
                        Gravity.NO_GRAVITY, 0, 0);
                return false;
            }
        });
        trans_icon.setOnLongClickListener(new OnLongClickListener() {

            public boolean onLongClick(View v) {
                initPopWindow();
                mLayoutGroup.removeAllViews();
                PinchableImageView imgBtn = new PinchableImageView(NewPlayActivity.this);
                imgBtn.setImageDrawable(((ImageView) v).getDrawable());
                imgBtn.setX(v.getX());
                imgBtn.setY(v.getY());
                imgBtn.setClickable(true);
                imgBtn.setOnTouchListener(touchListener2);
                mLayoutGroup.addView(imgBtn);
                mLayoutGroup.setFocusableInTouchMode(true);
                imgBtn.requestFocus();
                mPop.showAtLocation(
                        NewPlayActivity.this.trans_layout,
                        Gravity.NO_GRAVITY, 0, 0);
                return false;
            }
        });
    }

    OnTouchListener touchListener = new OnTouchListener() {
        int temp[] = new int[] { 0, 0 };

        public boolean onTouch(View arg0, MotionEvent arg1) {
            // TODO Auto-generated method stub
            int eventAction = arg1.getAction();

            int x = (int) arg1.getRawX();
            int y = (int) arg1.getRawY();

            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    temp[0] = (int) arg1.getX();
                    temp[1] = (int) (y - arg0.getTop());
                    mLayoutGroup.bringChildToFront(arg0);
                    arg0.postInvalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    int left = x - temp[0];
                    int top = y - temp[1];
                    int right = left + arg0.getWidth();
                    int bottom = top + arg0.getHeight();
                    arg0.layout(left, top, right, bottom);
                    arg0.postInvalidate();
                    break;

                // 终止触摸时刻
                case MotionEvent.ACTION_UP:
                    int stopX = (int) arg0.getX();
                    int stopY = (int) arg0.getY();
//                    int stopX = (int) arg0.getX() + arg0.getWidth() / 2;
//                    int stopY = (int) arg0.getY() + arg0.getHeight() / 2;
                    int mllX = (int) main_ll.getX();
                    int mllY = (int) main_ll.getY();
                    int mllW = main_ll.getWidth();
                    int mllH = main_ll.getHeight();
                    int senseWidth = sense_layout.getWidth();
                    if (stopX > mllX && stopX < (mllX + mllW) && stopY > mllY
                            && stopY < (mllY + mllH)) {
                        PinchableImageView imageView = new PinchableImageView(NewPlayActivity.this);
                        imageView.setImageDrawable(((PinchableImageView) arg0).getDrawable());
                        RelativeLayout.LayoutParams stop_pos = new RelativeLayout.LayoutParams(500,500);
                        //stop_pos.addRule(RelativeLayout.CENTER_IN_PARENT, -1);
                        stop_pos.setMargins(x-senseWidth,y-50,x-senseWidth+500, y+450);
                        main_ll.addView(imageView,stop_pos);
                    }
                    mPop.dismiss();
                    break;
            }
            return false;
        }
    };
    OnTouchListener touchListener2 = new OnTouchListener() {
        int temp[] = new int[] { 0, 0 };

        public boolean onTouch(View arg0, MotionEvent arg1) {
            // TODO Auto-generated method stub
            int eventAction = arg1.getAction();

            int x = (int) arg1.getRawX();
            int y = (int) arg1.getRawY();

            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    temp[0] = (int) arg1.getX();
                    temp[1] = (int) (y - arg0.getTop());
                    mLayoutGroup.bringChildToFront(arg0);
                    arg0.postInvalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    int left = x - temp[0];
                    int top = y - temp[1];
                    int right = left + arg0.getWidth();
                    int bottom = top + arg0.getHeight();
                    arg0.layout(left, top, right, bottom);
                    arg0.postInvalidate();
                    break;

                // 终止触摸时刻
                case MotionEvent.ACTION_UP:
                    int stopX = (int) arg0.getX();
                    int stopY = (int) arg0.getY();
//                    int stopX = (int) arg0.getX() + arg0.getWidth() / 2;
//                    int stopY = (int) arg0.getY() + arg0.getHeight() / 2;
                    int mllX = (int) main_ll.getX();
                    int mllY = (int) main_ll.getY();
                    int mllW = main_ll.getWidth();
                    int mllH = main_ll.getHeight();
                    int senseWidth = trans_layout.getWidth();
                    if (stopX > mllX && stopX < (mllX + mllW) && stopY > mllY
                            && stopY < (mllY + mllH)) {
                        PinchableImageView imageView = new PinchableImageView(NewPlayActivity.this);
                        imageView.setImageDrawable(((PinchableImageView) arg0).getDrawable());
                        RelativeLayout.LayoutParams stop_pos = new RelativeLayout.LayoutParams(500,500);
                        //stop_pos.addRule(RelativeLayout.CENTER_IN_PARENT, -1);
                        stop_pos.setMargins(x-senseWidth,y-50,x-senseWidth+500, y+450);
                        main_ll.addView(imageView,stop_pos);
                    }
                    mPop.dismiss();
                    break;
            }
            return false;
        }
    };

    private void initPopWindow() {
        if (mPop == null) {
            mPop = new PopupWindow(mLayoutGroup, LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT, true);
        }
        if (mPop.isShowing()) {
            mPop.dismiss();
        }
    }
}
