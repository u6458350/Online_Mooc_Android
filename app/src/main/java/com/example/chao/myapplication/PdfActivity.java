package com.example.chao.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PdfActivity extends AppCompatActivity {
    private  TextView pdfname1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        pdfname1 = (TextView) findViewById(R.id.title_pdf1);
        pdfname1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PdfActivity.this,PdfShowActivity.class );
                startActivity(intent);
            }
        });

    }
}
