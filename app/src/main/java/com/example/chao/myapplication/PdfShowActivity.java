package com.example.chao.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class PdfShowActivity extends AppCompatActivity {
        private ImageButton readbutton2;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_pdf_show);

            if (getSupportActionBar() != null){
                getSupportActionBar().hide();
            }
            readbutton2 = (ImageButton) findViewById(R.id.readpdfBtn);
            readbutton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PdfShowActivity.this, PdfReadAvtivity.class);
                    startActivity(intent);
                }
            });
        }
    }
