package com.example.chao.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.chao.myapplication.model.Quiz;
import com.google.gson.Gson;
import com.http.HttpGetRequest;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by huidh123 on 2017/6/8.
 */

public class QuizActivity extends Activity {

    public static final String KEY_QUESTIONS = "KEY_QUESTIONS";
    private Button next;
    private TextView title;
    private TextView index;
    private RequestQueue mQueue;
    private ArrayList<Integer> questionsIDs;
    private int quizIndex = 0;
    private int correctCount = 0;

    private RadioGroup optionsGroup;
    private RadioButton option1;
    private RadioButton option2;
    private RadioButton option3;
    private RadioButton option4;
    OnNextClickListener onNextClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        questionsIDs = (ArrayList<Integer>) getIntent().getExtras().getSerializable(KEY_QUESTIONS);
        mQueue = Volley.newRequestQueue(this);

        initViews();
        refreshContent(questionsIDs.get(quizIndex++));
    }

    private void initViews() {
        next = (Button) findViewById(R.id.next);
        index = (TextView) findViewById(R.id.index);
        title = (TextView) findViewById(R.id.title);
        onNextClick = new OnNextClickListener();
        next.setOnClickListener(onNextClick);
        optionsGroup = (RadioGroup) findViewById(R.id.options);
        option1 = (RadioButton) findViewById(R.id.option1);
        option2 = (RadioButton) findViewById(R.id.option2);
        option3 = (RadioButton) findViewById(R.id.option3);
        option4 = (RadioButton) findViewById(R.id.option4);
    }

    private void refreshContent(int id) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", String.valueOf(id));
        mQueue.add(HttpGetRequest.create(Constants.QUERY_QUESTION, params, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                HttpResponse res = gson.fromJson(response, HttpResponse.class);
                if (res.getCode() == 0) {
                    Quiz quiz = gson.fromJson(res.getContent(), Quiz.class);
                    bindData(quiz);
                }
            }
        }));
    }

    private void bindData(Quiz quiz) {
        title.setText(quiz.getTitle());
        String[] options = quiz.getOptions().split("###");
        option1.setText(options[0]);
        option2.setText(options[1]);
        option3.setText(options[2]);
        option4.setText(options[3]);
        optionsGroup.clearCheck();
        onNextClick.setAnswer(Integer.parseInt(quiz.getAnswer()));
    }

    private void showResultDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setMessage("测试分数:" + (correctCount * 100 / questionsIDs.size()))
                .setTitle("测试完成")
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        QuizActivity.this.finish();
                    }
                }).create().show();
    }

    class OnNextClickListener implements View.OnClickListener {
        private int answer;

        public void setAnswer(int answer) {
            this.answer = answer;
        }

        @Override
        public void onClick(View view) {
            if (quizIndex < questionsIDs.size()) {
                int checkedRadioButtonId = optionsGroup.getCheckedRadioButtonId();
                int selectIndex = -1;
                if (checkedRadioButtonId == R.id.option1) {
                    selectIndex = 0;
                } else if (checkedRadioButtonId == R.id.option2) {
                    selectIndex = 1;
                } else if (checkedRadioButtonId == R.id.option3) {
                    selectIndex = 2;
                } else if (checkedRadioButtonId == R.id.option4) {
                    selectIndex = 3;
                }
                if (selectIndex == -1) {
                    Toast.makeText(QuizActivity.this, "必须选择一个选项", Toast.LENGTH_LONG).show();
                    return;
                }
                if (answer == selectIndex) {
                    correctCount++;
                }

                refreshContent(questionsIDs.get(quizIndex++));
            } else {
                showResultDialog();
            }
        }
    }
}
