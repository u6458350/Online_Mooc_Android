package com.example.chao.myapplication;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.http.HttpGetRequest;

import java.util.HashMap;


public class RegisterActivity extends AppCompatActivity {
    private Button reg;
    private EditText username, password;
    private SharedPreferences share;//声明SharedPreferences
    private RequestQueue mRquestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        intiview();

        mRquestQueue = Volley.newRequestQueue(this);

        reg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String nam = username.getText().toString();
                final String pas = password.getText().toString();
                //判读信息是否空
                if (nam.trim().equals("") || pas.trim().equals("")) {
                    Toast.makeText(RegisterActivity.this, "注册时，用户名和密码都不能为空", Toast.LENGTH_LONG).show();
                    return;//为空就会返回
                }
                //进入注册的Dialog
                Dialog dialog = new AlertDialog.Builder(RegisterActivity.this)
                        .setTitle("注册")
                        .setMessage("确定注册用户吗")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                //
//                                share=getSharedPreferences("info", Activity.MODE_PRIVATE);
//                                SharedPreferences.Editor edit=share.edit();
//                                edit.putString("username", nam);
//                                edit.putString("password", pas);
//                                edit.apply();

                                HashMap<String , Object> postHeader = new HashMap<>();
                                postHeader.put("username", nam);
                                postHeader.put("password", pas);
                                mRquestQueue.add(HttpGetRequest.create(Constants.REGISTER, postHeader, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String s) {
                                        Gson gson = new Gson();
                                        HttpResponse response = gson.fromJson(s, HttpResponse.class);
                                        if (response.getCode() != 0) {
                                            Toast.makeText(RegisterActivity.this, response.getMsg(), Toast.LENGTH_LONG).show();
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(RegisterActivity.this, response.getMsg(), Toast.LENGTH_LONG).show();
                                            dialog.dismiss();
                                            RegisterActivity.this.finish();
                                        }
                                    }
                                }));


                            }
                        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                            }
                        }).create();
                dialog.show();
            }
        });

    }


    private void intiview() {
        // TODO Auto-generated method stub
        reg = (Button) findViewById(R.id.register_register);
        username = (EditText) findViewById(R.id.editUsername_register);
        password = (EditText) findViewById(R.id.editPassword_register);
    }
}


/*
        loginBtn = (Button) findViewById(R.id.loginBtn_login);
        registerBtn = (Button) findViewById(R.id.registerBtn_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,ResourceActivity.class);
                startActivity(intent);
            }
        });
        registerBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
*/
