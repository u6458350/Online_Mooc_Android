package com.example.chao.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.chao.myapplication.Adapter.ResourceItemAdapter;
import com.example.chao.myapplication.model.Resource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.http.HttpGetRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResourceActivity extends AppCompatActivity {

    private ImageView imageButton1, imageButton2, imageButton3;
    private Button exitButton, testbutton_play;
    private View view_resource, view_quiz, view_info;
    private ViewPager viewPager;
    private List<View> viewList;
    private BottomNavigationView navigation;
    private RoundImageView headIcon;
    private TextView infoTextview, messageTextview, downloadTextview, uploadTextview;
    private RequestQueue mQueue;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_resource:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_quiz:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_userInfo:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        mQueue = Volley.newRequestQueue(this);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        LayoutInflater inflater = getLayoutInflater();
        view_resource = inflater.inflate(R.layout.resource_layout, null);
        view_quiz = inflater.inflate(R.layout.quiz_layout, null);
        view_info = inflater.inflate(R.layout.infomation_layout, null);
        headIcon = (RoundImageView) view_info.findViewById(R.id.headIcon_information);
        infoTextview = (TextView) view_info.findViewById((R.id.info_information));
        messageTextview = (TextView) view_info.findViewById(R.id.message_information);
        uploadTextview = (TextView) view_info.findViewById(R.id.upload_information);
        downloadTextview = (TextView) view_info.findViewById(R.id.download_information);

        viewList = new ArrayList<>();
        viewList.add(view_resource);
        viewList.add(view_quiz);
        viewList.add(view_info);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                navigation.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        PagerAdapter pagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            public int getCount() {
                return viewList.size();
            }

            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView(viewList.get(position));
            }

            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(viewList.get(position));
                return viewList.get(position);
            }
        };
        viewPager.setAdapter(pagerAdapter);

        imageButton1 = (ImageView) view_resource.findViewById(R.id.video_resource);
        imageButton2 = (ImageView) view_resource.findViewById(R.id.word_resource);
        imageButton3 = (ImageView) view_resource.findViewById(R.id.pdf_resource);
        exitButton = (Button) view_info.findViewById(R.id.exit);


        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, VideoActivity.class);
                Bundle extras = new Bundle();
                extras.putInt(VideoActivity.KEY_RES_TYPE, 1);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, VideoActivity.class);
                Bundle extras = new Bundle();
                extras.putInt(VideoActivity.KEY_RES_TYPE, 2);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, VideoActivity.class);
                Bundle extras = new Bundle();
                extras.putInt(VideoActivity.KEY_RES_TYPE, 3);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        infoTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, InformationActivity.class);
                startActivity(intent);
            }
        });
        messageTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, MessageActivity.class);
                startActivity(intent);
            }
        });
/*        likeTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, LikeActivity.class);
                startActivity(intent);
            }
        });*/
        downloadTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, DownloadActivity.class);
                startActivity(intent);
            }
        });
        uploadTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResourceActivity.this, UploadActivity.class);
                startActivity(intent);
            }
        });

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        initNewsList();
        initTestView();
    }

    private void initNewsList() {
        ListView mNewsList = (ListView) view_resource.findViewById(R.id.newsList);
        final ResourceItemAdapter newsAdapter = new ResourceItemAdapter(this);
        mNewsList.setAdapter(newsAdapter);
        mNewsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int resId = ((Resource) adapterView.getAdapter().getItem(i)).getId();
                Intent intent = new Intent(ResourceActivity.this, VideoShowActivity.class);
                Bundle extras = new Bundle();
                extras.putInt(VideoShowActivity.KEY_RES_ID, resId);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });

        HashMap params = new HashMap();
        params.put("type", 4);
        mQueue.add(HttpGetRequest.create(Constants.QUERY_RES_LIST, params, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                HttpResponse res = gson.fromJson(response, HttpResponse.class);
                if (res.getCode() == 0) {
                    ArrayList<Resource> resList = gson.fromJson(res.getContent(), new TypeToken<ArrayList<Resource>>() {
                    }.getType());
                    newsAdapter.updateData(resList);
                }
            }
        }));
    }

    private void initTestView() {
        testbutton_play = (Button) view_quiz.findViewById(R.id.testbutton);
        final Spinner spinner_type = (Spinner) view_quiz.findViewById(R.id.type);
        spinner_type.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{"物联网概论", "无线传感器网络", "嵌入式"}));
        final EditText count = (EditText) view_quiz.findViewById(R.id.questions_count);
        testbutton_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(count.getText())) {
                    Toast.makeText(ResourceActivity.this, "未输入试题数量", Toast.LENGTH_LONG).show();
                    return;
                }
                int quesCount = Integer.valueOf(count.getText().toString());
                if (quesCount <= 0) {
                    Toast.makeText(ResourceActivity.this, "试题数量无效", Toast.LENGTH_LONG).show();
                    return;
                }
                int type = spinner_type.getSelectedItemPosition();
                if (quesCount <= -1) {
                    Toast.makeText(ResourceActivity.this, "选择一个类型", Toast.LENGTH_LONG).show();
                    return;
                }
                createTest(quesCount, type);
            }
        });
    }

    private void createTest(int count, int type) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("count", String.valueOf(count));
        params.put("type", String.valueOf(type));
        mQueue.add(HttpGetRequest.create(Constants.CREATE_TEST, params, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                HttpResponse res = gson.fromJson(response, HttpResponse.class);
                if (res.getCode() == 0) {
                    ArrayList<Integer> questions = gson.fromJson(res.getContent(), new TypeToken<ArrayList<Integer>>() {
                    }.getType());

                    Intent intent = new Intent(ResourceActivity.this, QuizActivity.class);
                    Bundle extras = new Bundle();
                    extras.putSerializable(QuizActivity.KEY_QUESTIONS, questions);
                    intent.putExtras(extras);
                    startActivity(intent);
                } else {
                    Toast.makeText(ResourceActivity.this, "试卷生成失败:" + res.getMsg(), Toast.LENGTH_LONG).show();
                }
            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Glide.with(ResourceActivity.this).load(Constants.IP_ADDRESS + Constants.mSignInUser.getHeadicon()).into(headIcon);
    }
}
