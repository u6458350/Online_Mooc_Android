package com.example.chao.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.http.multipart.FilePart;
import com.android.internal.http.multipart.Part;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.chao.myapplication.Utils.FileUtils;
import com.google.gson.Gson;
import com.http.HttpPostRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

public class UploadDetailActivity extends AppCompatActivity {

    protected static final int FILE_SELECT_CODE = 0;
    private String selectedFilePath;
    private RequestQueue queue;

    private TextView uploadFile;
    private Button submit;
    private Spinner resourceType;
    private EditText title;
    private EditText details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_detail);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        queue = Volley.newRequestQueue(this);

        uploadFile = (TextView) findViewById(R.id.chooseBt_upload_detail);
        uploadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openFileManager = new Intent(Intent.ACTION_GET_CONTENT);
                openFileManager.setType("*/*");
                openFileManager.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(Intent.createChooser(openFileManager, "Select a File to Upload"), FILE_SELECT_CODE);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(UploadDetailActivity.this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        submit = (Button) findViewById(R.id.confirmBt_upload_detail);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadFile(selectedFilePath);
            }
        });
        title = (EditText) findViewById(R.id.titleET_upload_detail);
        details = (EditText) findViewById(R.id.contentET_upload_detail);

        resourceType = (Spinner) findViewById(R.id.typeSpinner_upload_detail);
        resourceType.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, new String[]{"视频", "文档", "PDF"}));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    selectedFilePath = FileUtils.getPath(this, uri);
                    Toast.makeText(UploadDetailActivity.this, String.format("已选择文件:%s", selectedFilePath), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void uploadFile(String filePath) {
        final File uploadFile = new File(filePath);
        if (selectedFilePath == null || !uploadFile.exists()) {
            Toast.makeText(UploadDetailActivity.this, "未选择文件", Toast.LENGTH_SHORT).show();
            return;
        }

        final int type = resourceType.getSelectedItemPosition() + 1;
        if (type == 0) {
            Toast.makeText(UploadDetailActivity.this, "请选择类型", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(title.getText())) {
            Toast.makeText(UploadDetailActivity.this, "请输入标题", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(details.getText())) {
            Toast.makeText(UploadDetailActivity.this, "请输入详细信息", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            Part[] parts = new Part[1];
            parts[0] = new FilePart("files", uploadFile);
            queue.add(new MultipartRequest(Constants.UPLOAD_URL, parts, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    final Gson gson = new Gson();
                    HttpResponse res = gson.fromJson(response, HttpResponse.class);
                    if (res.getCode() == 0) {
                        final String uploadedFileUrl = gson.fromJson(res.getContent(), String.class);
                        Toast.makeText(UploadDetailActivity.this, String.format("文件上传成功:%s", uploadedFileUrl), Toast.LENGTH_SHORT).show();

//                      上传详细信息
                        HashMap<String, String> params = new HashMap<>();
                        params.put("uploadUserId", String.valueOf(Constants.mSignInUser.getId()));
                        params.put("url", uploadedFileUrl);
                        params.put("title", title.getText().toString());
                        params.put("details", title.getText().toString());
                        params.put("type", String.valueOf(type));

                        queue.add(HttpPostRequest.create(Constants.UPLOAD_RESOURCE, params, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                HttpResponse res = gson.fromJson(response, HttpResponse.class);
                                if (res.getCode() == 0) {
                                    Toast.makeText(UploadDetailActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    UploadDetailActivity.this.finish();
                                } else {
                                    Toast.makeText(UploadDetailActivity.this, res.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }));
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
