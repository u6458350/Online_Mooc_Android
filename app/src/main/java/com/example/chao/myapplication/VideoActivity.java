package com.example.chao.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.chao.myapplication.Adapter.ResourceItemAdapter;
import com.example.chao.myapplication.model.Resource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.http.HttpGetRequest;

import java.util.ArrayList;
import java.util.HashMap;

public class VideoActivity extends AppCompatActivity {
    public static final String KEY_RES_TYPE = "KEY_RES_TYPE";
    ResourceItemAdapter resAdapter;
    private ListView resList;
    private int resourceType;
    private RequestQueue mQueue;
    private SearchView search;
    private Button video_upload;
    private TextView videotitle_video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        resourceType = getIntent().getExtras().getInt(KEY_RES_TYPE);

        mQueue = Volley.newRequestQueue(this);
        resAdapter = new ResourceItemAdapter(this);
        search = (SearchView) findViewById(R.id.search_video);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!TextUtils.isEmpty(s)) {
                    search(s);
                } else {
                    Toast.makeText(VideoActivity.this, "请输入搜索关键字", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        initActionBar();
        resList = (ListView) findViewById(R.id.resList);
        resList.setAdapter(resAdapter);
        resList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int resId = ((Resource) adapterView.getAdapter().getItem(i)).getId();
                Intent intent = new Intent(VideoActivity.this, VideoShowActivity.class);
                Bundle extras = new Bundle();
                extras.putInt(VideoShowActivity.KEY_RES_ID, resId);
                intent.putExtras(extras);
                startActivity(intent);
            }
        });
        video_upload = (Button) findViewById(R.id.video_upload);
        video_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(VideoActivity.this, UploadDetailActivity.class));
            }
        });
        refreshResList();
    }

    private void initActionBar() {
        videotitle_video = (TextView) findViewById(R.id.videotitle_video);
        if (resourceType == 1) {
            videotitle_video.setText("视频");
        } else if (resourceType == 2) {
            videotitle_video.setText("文档");
        } else if (resourceType == 3) {
            videotitle_video.setText("PDF");
        } else if (resourceType == 4) {
            videotitle_video.setText("新闻");
        }
    }

    private void refreshResList() {
        HashMap params = new HashMap();
        params.put("type", resourceType);
        mQueue.add(HttpGetRequest.create(Constants.QUERY_RES_LIST, params, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                HttpResponse res = gson.fromJson(response, HttpResponse.class);
                if (res.getCode() == 0) {
                    ArrayList<Resource> resItemList = gson.fromJson(res.getContent(), new TypeToken<ArrayList<Resource>>() {
                    }.getType());
                    resAdapter.updateData(resItemList);
                }
            }
        }));
    }

    private void search(String keyword) {
        Toast.makeText(this, String.format("搜索:%s结果如下", keyword), Toast.LENGTH_LONG).show();
        HashMap params = new HashMap();
        params.put("type", resourceType);
        params.put("keyword", keyword);
        mQueue.add(HttpGetRequest.create(Constants.SEARCH, params, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                HttpResponse res = gson.fromJson(response, HttpResponse.class);
                if (res.getCode() == 0) {
                    ArrayList<Resource> resItemList = gson.fromJson(res.getContent(), new TypeToken<ArrayList<Resource>>() {
                    }.getType());
                    resAdapter.updateData(resItemList);
                }
            }
        }));
    }
}
