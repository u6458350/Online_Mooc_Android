package com.example.chao.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.chao.myapplication.Utils.FileUtils;
import com.example.chao.myapplication.model.Comments;
import com.example.chao.myapplication.model.Resource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.http.HttpGetRequest;
import com.http.HttpPostRequest;

import org.vudroid.core.utils.MD5StringUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VideoShowActivity extends AppCompatActivity {
    public static final String KEY_RES_ID = "KEY_RES_ID";
    Resource resDetails;
    private int resId;

    private RequestQueue mQueue;
    private ImageButton playbutton;
    private TextView title;
    private TextView details;
    private LinearLayout mCommentInput;
    private Button comment;
    private EditText mCommentInputContent;
    private Button mCommentInputSubmit;
    private ListView commentsList;
    private CommentAdapter commentAdapter;
    private TextView videotitle_videoshow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_show);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        resId = getIntent().getExtras().getInt(KEY_RES_ID);
        mQueue = Volley.newRequestQueue(this);

        playbutton = (ImageButton) findViewById(R.id.playvideoBtn);
        title = (TextView) findViewById(R.id.videoname_videoshow);
        details = (TextView) findViewById(R.id.information_videoshow);
        mCommentInput = (LinearLayout) findViewById(R.id.comment_input);
        mCommentInputContent = (EditText) findViewById(R.id.comment_input_content);
        mCommentInputSubmit = (Button) findViewById(R.id.comment_input_submit);
        commentsList = (ListView) findViewById(R.id.comments);
        commentAdapter = new CommentAdapter(this);
        commentsList.setAdapter(commentAdapter);
        comment = (Button) findViewById(R.id.comment_resource);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCommentLayout(true, -1);
            }
        });
        commentsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Comments item = (Comments) adapterView.getAdapter().getItem(i);
                showOptionsDialog(item.getId());
            }
        });
        playbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playResource();
            }
        });

        refreshData(resId);
    }

    private void refreshData(int resId) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("id", resId);
        mQueue.add(HttpGetRequest.create(Constants.QUERY_RES_DETAIL, params, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                HttpResponse res = gson.fromJson(response, HttpResponse.class);
                if (res.getCode() == 0) {
                    resDetails = gson.fromJson(res.getContent(), Resource.class);
                    title.setText(resDetails.getTitle());
                    details.setText(resDetails.getDetails());
                    initActionBar(resDetails.getType());
                    refreshComments();
                }
            }
        }));
    }

    private void refreshComments() {
        HashMap params = new HashMap<String, String>();
        params.put("resId", String.valueOf(resDetails.getId()));
        mQueue.add(HttpGetRequest.create(Constants.QUERY_COMMENT, params, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                HttpResponse res = new Gson().fromJson(response, HttpResponse.class);
                if (res.getCode() == 0) {
                    List<Comments> commentsList = new Gson().fromJson(res.getContent(), new TypeToken<ArrayList<Comments>>() {
                    }.getType());

                    commentAdapter.uploadData(sortComments(commentsList));
                }
            }
        }));
    }

    private void playResource() {
        if (resDetails.getType() == 1) {
            Intent intent = new Intent(VideoShowActivity.this, VideoPlayActivity.class);
            Bundle extras = new Bundle();
            extras.putString(VideoPlayActivity.KEY_PATH, resDetails.getUrl());
            intent.putExtras(extras);
            startActivity(intent);
        } else if (resDetails.getType() == 2) {
            DownloadFileTask fileTask = new DownloadFileTask();
            fileTask.execute(Constants.IP_ADDRESS + resDetails.getUrl());
        } else if (resDetails.getType() == 3) {
            DownloadFileTask fileTask = new DownloadFileTask();
            fileTask.execute(Constants.IP_ADDRESS + resDetails.getUrl());
        }
    }

    private void showOptionsDialog(final int replyId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(new String[]{"回复", "取消"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    showCommentLayout(false, replyId);
                } else if (i == 1) {
                    dialogInterface.dismiss();
                }
            }
        }).create().show();
    }

    private void showCommentLayout(boolean isComment, final int replyId) {
        mCommentInput.setVisibility(View.VISIBLE);
        clearCommentInput();
        if (isComment) {
            mCommentInputContent.setHint(String.format("输入 %s 评论", resDetails.getTitle()));
        } else {
            mCommentInputContent.setHint(String.format("回复 %s", "评论"));
        }

        mCommentInputSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Editable text = mCommentInputContent.getText();
                if (TextUtils.isEmpty(text)) {
                    Toast.makeText(VideoShowActivity.this, "请输入内容", Toast.LENGTH_LONG).show();
                    return;
                }

                HashMap<String, String> params = new HashMap();
                params.put("content", text.toString());
                params.put("resId", String.valueOf(resDetails.getId()));
                params.put("userId", String.valueOf(Constants.mSignInUser.getId()));
                params.put("replyId", String.valueOf(replyId));
                mQueue.add(HttpPostRequest.create(Constants.ADD_COMMENT, params, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        HttpResponse res = new Gson().fromJson(response, HttpResponse.class);
                        if (res.getCode() == 0) {
                            Toast.makeText(VideoShowActivity.this, "评论成功", Toast.LENGTH_LONG).show();
                            hideCommentLayout();

                            refreshComments();
                        }
                    }
                }));

            }
        });
    }

    private void hideCommentLayout() {
        mCommentInput.setVisibility(View.INVISIBLE);
        clearCommentInput();
    }

    private void clearCommentInput() {
        mCommentInputContent.setText("");
    }

    private List<Comments> sortComments(List<Comments> source) {
        Map<Integer, List<Comments>> replysMap = new HashMap<>();
        try {
            for (Comments item : source) {
                List<Comments> replys = replysMap.get(item.getReplyId());
                if (replys == null) {
                    replys = new ArrayList();
                    replysMap.put(item.getReplyId(), replys);
                }
                replys.add(item.clone());
            }
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        Comments comm = new Comments();
        comm.setId(-1);
        return soreCore(new ArrayList<Comments>(), replysMap, comm);
    }

    private List<Comments> soreCore(List<Comments> res, Map<Integer, List<Comments>> replysMap, Comments comm) {
        if (replysMap.get(comm.getId()) == null) {
            return res;
        }
        int floor = 1;
        List<Comments> comments = replysMap.get(comm.getId());
        for (Comments item : comments) {
            res.add(item);
            if (comm.getId() == -1) {
                item.setFloor(String.valueOf(floor++));
            } else {
                item.setReplyUsername(comm.getUsername());
            }
            res = soreCore(res, replysMap, item);
        }
        return res;
    }


    class DownloadFileTask extends AsyncTask<String, Void, String> {

        private ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... strs) {
            String fileUrl = strs[0];
            String fileName = MD5StringUtil.md5StringFor(fileUrl);
            String savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + FileUtils.SAVE_FILE_PATH;
            String downloadFilePath = savePath + fileName;

            File saveDir = new File(savePath);
            if (!saveDir.exists()) {
                saveDir.mkdirs();
            }

            File downloadFile = new File(downloadFilePath);
            if (downloadFile.exists()) {
                return downloadFilePath;
            } else {
                FileUtils.downloadFile(fileUrl, savePath, fileName);
                return downloadFilePath;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = createDialog();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            Uri uri = Uri.fromFile(new File(s));
            Intent intent = new Intent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "application/msword");
            startActivity(intent);
        }

        private ProgressDialog createDialog() {
            ProgressDialog dialog = new ProgressDialog(VideoShowActivity.this);
            dialog.setMessage("正在下载文件");
            return dialog;
        }
    }

    class CommentAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private List<Comments> mDatas;

        public CommentAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        public void uploadData(List<Comments> data) {
            this.mDatas = data;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mDatas != null ? mDatas.size() : 0;
        }

        @Override
        public Object getItem(int i) {
            return mDatas.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View rootView = mInflater.inflate(R.layout.item_comment, null);
            TextView floor = (TextView) rootView.findViewById(R.id.floor);
            TextView content = (TextView) rootView.findViewById(R.id.content);
            TextView username = (TextView) rootView.findViewById(R.id.username);

            Comments comments = mDatas.get(i);
            if (comments.getReplyId() != -1) {
                rootView.setPadding(120, 0, 0, 0);
                floor.setVisibility(View.GONE);
                content.setText("回复 " + comments.getReplyUsername() + ":" + comments.getContent());
                username.setText(comments.getUsername());
            } else {
                floor.setText("#" + comments.getFloor());
                content.setText(comments.getContent());
                username.setText(comments.getUsername());
            }
            return rootView;
        }
    }


    private void initActionBar(int type) {
        videotitle_videoshow = (TextView) findViewById(R.id.videotitle_videoshow);
        if (type == 1) {
            videotitle_videoshow.setText("视频");
        } else if (type == 2) {
            videotitle_videoshow.setText("文档");
        } else if (type == 3) {
            videotitle_videoshow.setText("PDF");
        } else if (type == 4) {
            videotitle_videoshow.setText("新闻");
            playbutton.setVisibility(View.INVISIBLE);
        }
    }

}
