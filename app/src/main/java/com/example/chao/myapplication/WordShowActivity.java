package com.example.chao.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class WordShowActivity extends AppCompatActivity {
    private ImageButton readbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_show);

        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        readbutton = (ImageButton) findViewById(R.id.readwordBtn);
        readbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordShowActivity.this, WordReadActivity.class);
                startActivity(intent);
            }
        });
    }
}
