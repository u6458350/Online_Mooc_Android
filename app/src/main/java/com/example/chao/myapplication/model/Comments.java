package com.example.chao.myapplication.model;

/**
 * Created by huidh123 on 2017/6/7.
 */

public class Comments {
    private int id;
    private String content;
    private String username;
    private int userId;
    private int replyId;
    private String replyUsername = "";

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    private String floor = "";

    public Comments(int id, String content, String username, int userId, int replyId) {
        this.id = id;
        this.content = content;
        this.username = username;
        this.userId = userId;
        this.replyId = replyId;
    }

    public Comments() {
    }

    public String getReplyUsername() {
        return replyUsername;
    }

    public void setReplyUsername(String replyUsername) {
        this.replyUsername = replyUsername;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getReplyId() {
        return replyId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    @Override
    public Comments clone() throws CloneNotSupportedException {
        return new Comments(id , content , username , userId , replyId);
    }
}
