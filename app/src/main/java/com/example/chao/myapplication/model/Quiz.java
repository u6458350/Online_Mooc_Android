package com.example.chao.myapplication.model;

/**
 * Created by huidh123 on 2017/6/8.
 */

public class Quiz {
    private int id;
    private String title;
    private String answer;
    private String options;
    private int type;

    public Quiz() {
    }

    public Quiz(int id, String title, String answer, String options, int type) {
        this.id = id;
        this.title = title;
        this.answer = answer;
        this.options = options;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
