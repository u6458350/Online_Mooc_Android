package com.example.chao.myapplication.model;

/**
 * Created by huidh123 on 2017/6/7.
 */

public class Resource {
    private int id;
    private String title;
    private String details;
    private int type;
    private String url;

    public Resource() {
    }

    public Resource(int id, String title, String details, int type, String url) {
        this.id = id;
        this.title = title;
        this.details = details;
        this.type = type;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
