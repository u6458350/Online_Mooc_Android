package com.http;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chao on 2017/5/5.
 */

public class HttpGetRequest {

    public static StringRequest create(String url, HashMap<String, Object> params, Response.Listener listener) {
        StringBuilder sb = new StringBuilder(url).append("?");
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        sb.deleteCharAt(sb.length() - 1);
        return new StringRequest(Request.Method.GET, sb.toString(), listener, null);
    }
}
