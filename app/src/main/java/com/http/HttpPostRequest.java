package com.http;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chao on 2017/5/5.
 */

public class HttpPostRequest {

    public static StringRequest create(String url, final HashMap<String, String> params, Response.Listener listener) {
        return new StringRequest(Request.Method.POST, url, listener, null) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
    }
}
